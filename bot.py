# -*- coding: utf-8 -*-
__author__ = 'zax'

import config
import telebot
import cherrypy
from breakstuff import BreakStuff


WEBHOOK_HOST = '185.146.171.58'
WEBHOOK_PORT = 8443  # 443, 80, 88 or 8443 (port need to be 'open')
WEBHOOK_LISTEN = '0.0.0.0'  # In some VPS you may need to put here the IP addr

WEBHOOK_SSL_CERT = 'webhook_cert.pem'  # Path to the ssl certificate
WEBHOOK_SSL_PRIV = 'webhook_pkey.pem'  # Path to the ssl private key

WEBHOOK_URL_BASE = "https://%s:%s" % (WEBHOOK_HOST, WEBHOOK_PORT)
WEBHOOK_URL_PATH = "/%s/" % config.token


bot = telebot.TeleBot(config.token)
break_stuff = BreakStuff()


class WebhookServer(object):
    @cherrypy.expose
    def index(self):
        if 'content-length' in cherrypy.request.headers and \
                        'content-type' in cherrypy.request.headers and \
                        cherrypy.request.headers['content-type'] == 'application/json':
            length = int(cherrypy.request.headers['content-length'])
            json_string = cherrypy.request.body.read(length).decode("utf-8")
            update = telebot.types.Update.de_json(json_string)
            # Эта функция обеспечивает проверку входящего сообщения
            bot.process_new_updates([update])
            return ''
        else:
            raise cherrypy.HTTPError(403)

# TODO async stuff
# TODO send images without saving to disk
# TODO logging
# TODO some of message options can be None
# TODO team can register only using teamname and unique id from en
# TODO filter users by team (one team - one player) (if one is registered as team, block its other teammates)


@bot.message_handler(commands=['help'])
def help_message(message):
    bot.send_message(message.chat.id, config.help_str)


@bot.message_handler(regexp=['^newgame /w{10}$'])
def new_game(message):
    # TODO LOG or IGNORE
    # break_stuff.db.add_message(message)

    # try to register
    res, photo = break_stuff.new_game(message)

    # process register status
    if res == -3:
        bot.send_message(message.chat.id, "Something wrong, try again")
    elif res == -2:
        bot.send_message(message.chat.id, "Неверный код")
    elif res == -1:
        bot.send_message(message.chat.id, 'Ваша команда уже запустила игру')
    elif res == 0:
        bot.send_message(message.chat.id, 'Ваша команда уже прошла уровень')
    else:  # reg == 1
        bot.send_message(message.chat.id, 'go koroch')
        photo = open('img' + str(message.chat.id) + '.jpg', 'rb')
        bot.send_photo(message.chat.id, photo)
        photo.close()


# TODO codes lengths
@bot.message_handler(regexp="^\d{1,2}[А-Яа-я]{2,12}\d{2} \d{1,2}[А-Яа-я]{2,12}\d{2}$")
def move(message):
    # TODO LOG or IGNORE
    # break_stuff.db.add_message(message)

    if not break_stuff.player_registered(message):
        # TODO reply or ignore
        # bot.send_message(message.chat.id, 'Register first')
        pass
    else:
        try:
            code1, code2 = message.text.split(' ')
        except (ValueError, TypeError):
            bot.send_message(message.chat.id, "Something wrong, try again")
            return

        # try to make move
        res, photo, collapsed = break_stuff.move(message, code1, code2)

        # process move result
        if res == -3:
            bot.send_message(message.chat.id, 'Коды неверные')
        elif res == -2:
            bot.send_message(message.chat.id, 'Некоторые из передвигаемых ячеек пусты')
        elif res == -1:
            bot.send_message(message.chat.id, 'Передвигаемые ячейки не соседние')
        elif res == 0:
            # move successful & end game
            break_stuff.stop_game(message.chat.id)
            photo = open('img' + str(message.chat.id) + '.jpg', 'rb')
            bot.send_photo(message.chat.id, photo)
            bot.send_message(message.chat.id, collapsed)
            bot.send_message(message.chat.id, 'Азаза, КАНАЛЬЯ! проходной код: ' + break_stuff.final_code)
        else:  # res == 1
            # move successful
            photo = open('img'+str(message.chat.id)+'.jpg', 'rb')
            bot.send_photo(message.chat.id, photo)
            bot.send_message(message.chat.id, collapsed)
            photo.close()


@bot.message_handler(func=lambda message: True, content_types=["text"])
def other(message):
    # TODO LOG or IGNORE
    # break_stuff.db.add_message(message)

    if not break_stuff.player_registered(message.chat.id):
        # TODO some reply or ignore
        # bot.send_message(message.chat.id, 'Register first')
        pass
    else:
        bot.send_message(message.chat.id, "Чтобы поменять местами шарики под номерами i, j, отправьте сообщение 'iслово11 jслово66', \n" \
                                          "где iслово11 и jслово66 - коды по меткам i, j")


# Remove webhook, it fails sometimes the set if there is a previous webhook
bot.remove_webhook()

# Set webhook
bot.set_webhook(url=WEBHOOK_URL_BASE + WEBHOOK_URL_PATH,
                certificate=open(WEBHOOK_SSL_CERT, 'r'))

# Start cherrypy server
cherrypy.config.update({
    'server.socket_host': WEBHOOK_LISTEN,
    'server.socket_port': WEBHOOK_PORT,
    'server.ssl_module': 'builtin',
    'server.ssl_certificate': WEBHOOK_SSL_CERT,
    'server.ssl_private_key': WEBHOOK_SSL_PRIV
})

cherrypy.quickstart(WebhookServer(), WEBHOOK_URL_PATH, {'/': {}})
