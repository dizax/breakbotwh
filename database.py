# -*- coding: utf-8 -*-
# __author__ = 'zax'

import sqlite3 as mdb
import datetime


class Database:
    def __init__(self):
        # connect to db
        self.con = mdb.connect('log.db', check_same_thread=False)
        self.cur = self.con.cursor()

        # create tables (if don't exist)
        sql = """CREATE TABLE IF NOT EXISTS chats (
                    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                    chat_id INTEGER NOT NULL UNIQUE,
                    type TEXT NOT NULL,
                    title TEXT DEFAULT ""
                )"""
        self.cur.execute(sql)

        sql = """CREATE TABLE IF NOT EXISTS users (
                    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                    user_id INTEGER NOT NULL UNIQUE,
                    first_name TEXT DEFAULT "",
                    last_name TEXT DEFAULT "",
                    username TEXT DEFAULT ""
                )"""
        self.cur.execute(sql)

        # id <-> team_id
        sql = """CREATE TABLE IF NOT EXISTS teams (
                        id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                        team_id INTEGER NOT NULL,
                        team_name TEXT NOT NULL,
                        chat_id INTEGER NOT NULL,
                        is_active INTEGER NOT NULL
                    )"""
        self.cur.execute(sql)

        sql = """CREATE TABLE IF NOT EXISTS messages (
                    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                    chat_id INTEGER NOT NULL,
                    user_id INTEGER NOT NULL,
                    time DATETIME NOT NULL,
                    text TEXT NOT NULL
                )"""
        self.cur.execute(sql)

        sql = """CREATE TABLE IF NOT EXISTS moves (
                    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                    team_id INTEGER NOT NULL,
                    chat_id INTEGER NOT NULL,
                    user_id INTEGER NOT NULL,
                    time DATETIME NOT NULL,
                    cell1 INTEGER NOT NULL,
                    cell2 INTEGER NOT NULL,
                    collapsed_now INTEGER NOT NULL,
                    collapsed_all INTEGER NOT NULL,
                    moves_cnt INTEGER NOT NULL
                )"""
        self.cur.execute(sql)

    def disconnect(self):
        self.cur.close()
        self.con.close()

    def new_game(self, msg, team, team_id):
        title = "" if msg.chat.title is None else msg.chat.title
        first_name = "" if msg.from_user.first_name is None else msg.from_user.first_name
        last_name = "" if msg.from_user.last_name is None else msg.from_user.last_name
        username = "" if msg.from_user.username is None else msg.from_user.username

        sql1 = """INSERT OR IGNORE INTO chats(chat_id, type, title)
            VALUES ('%(chat_id)s', '%(type)s', '%(title)s')
            """ % {"chat_id": msg.chat.id, "type": msg.chat.type, "title": title}
        sql2 = """INSERT OR IGNORE INTO users(user_id, first_name, last_name, username)
            VALUES ('%(user_id)s', '%(first_name)s', '%(last_name)s', '%(username)s')
            """ % {"user_id": msg.from_user.id, "first_name": first_name,
                   "last_name": last_name, "username": username}
        sql3 = """INSERT OR IGNORE INTO teams(team_id, team_name, chat_id, is_active)
            VALUES ('%(team_id)s', '%(team_name)s', '%(chat_id)s', '%(is_active)s')
            """ % {"team_id": team_id, "team_name": team, "chat_id": msg.chat.id, "is_active": 1}

        try:
            self.cur.execute(sql1)
            self.cur.execute(sql2)
            self.cur.execute(sql3)
            self.con.commit()
        except mdb.Error:
            print "cant add chat and user in new game"
            self.con.rollback()

    def stop_game(self, team):
        sql1 = """UPDATE teams SET is_active=0 WHERE team_name='%s'""" % team
        try:
            self.cur.execute(sql1)
            self.con.commit()
        except mdb.Error:
            print "cant update teams"
            self.con.rollback()

    def add_message(self, msg):
        title = "" if msg.chat.title is None else msg.chat.title
        first_name = "" if msg.from_user.first_name is None else msg.from_user.first_name
        last_name = "" if msg.from_user.last_name is None else msg.from_user.last_name
        username = "" if msg.from_user.username is None else msg.from_user.username
        time = datetime.datetime.fromtimestamp(int(msg.date)).strftime('%Y-%m-%d %H:%M:%S')

        sql1 = """INSERT OR IGNORE INTO chats(chat_id, type, title)
            VALUES ('%(chat_id)s', '%(type)s', '%(title)s')
            """ % {"chat_id": msg.chat.id, "type": msg.chat.type, "title": title}
        sql2 = """INSERT OR IGNORE INTO users(user_id, first_name, last_name, username)
            VALUES ('%(user_id)s', '%(first_name)s', '%(last_name)s', '%(username)s')
            """ % {"user_id": msg.from_user.id, "first_name": first_name,
                   "last_name": last_name, "username": username}

        sql3 = """INSERT INTO messages(chat_id, user_id, time, text)
                VALUES ('%(chat_id)s', '%(user_id)s', '%(time)s', '%(text)s')
                """ % {"chat_id": msg.chat.id, "user_id": msg.from_user.id,
                       "time": time, "text": msg.chat.text}
        try:
            self.cur.execute(sql1)
            self.cur.execute(sql2)
            self.cur.execute(sql3)
            self.con.commit()
        except mdb.Error:
            print "cant add chat and user in add message"
            self.con.rollback()

    def add_move(self, msg, team_id, collapsed_now, collapsed_all, moves_cnt, cell1, cell2):
        # self.add_message(msg)

        time = datetime.datetime.fromtimestamp(int(msg.date)).strftime('%Y-%m-%d %H:%M:%S')
        sql = """INSERT INTO moves(team_id, chat_id, user_id, time, collapsed_now, collapsed_all, moves_cnt, cell1, cell2)
            VALUES ('%(team_id)s', '%(chat_id)s', '%(user_id)s', '%(time)s',
            '%(collapsed_now)s', '%(collapsed_all)s', '%(moves_cnt)s', '%(cell1)s', '%(cell2)s')
            """ % {"team_id": int(team_id), "chat_id": msg.chat.id, "user_id": msg.from_user.id, "time": time,
                   "collapsed_now": collapsed_now, "collapsed_all": collapsed_all, "moves_cnt": moves_cnt,
                   "cell1": cell1, "cell2": cell2}

        try:
            self.cur.execute(sql)
            self.con.commit()
        except mdb.Error:
            print "cant insert moves"
            self.con.rollback()

    def fetch_teams(self):
        sql = """SELECT
            """

    def stats_tmp(self, static):
        if static:
            sql = """
                SELECT game_id, username, min(moves_cnt) as minmv, collapsed_all
                FROM moves WHERE collapsed_all>85 and static=1 GROUP BY game_id ORDER BY minmv LIMIT 10;
            """
        else:
            sql = """
                SELECT game_id, username, min(moves_cnt) as minmv, collapsed_all
                FROM moves WHERE collapsed_all>85 and static=0 GROUP BY game_id ORDER BY minmv LIMIT 10;
            """

        try:
            self.cur.execute(sql)
            results = self.cur.fetchall()

            res = "Username | moves | collapsed_all\n"
            res += "\n".join([str(r[1])+" | "+str(r[2])+" | "+str(r[3]) for r in results])
            return res
        except mdb.Error:
            print "Error: unable to fetch stats"
            return "Netu koroch"
