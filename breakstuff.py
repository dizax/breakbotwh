# -*- coding: utf-8 -*-
# __author__ = 'zax'

from StringIO import StringIO
from PIL import Image, ImageDraw, ImageFont
from field import Field
from database import Database

# Объявляем переменные
ROWS = 10
COLS = 10
CELLS_CNT = ROWS*COLS
CELL_SIZE = 40
WIN_WIDTH = CELL_SIZE*ROWS  # Ширина
WIN_HEIGHT = CELL_SIZE*COLS  # Высота
DISPLAY = (WIN_WIDTH, WIN_HEIGHT)  # Группируем ширину и высоту в одну переменную
BACKGROUND_COLOR = (255, 255, 255)
COLORS = [(255, 50, 50), (0, 255, 0), (0, 255, 255), (255, 200, 0), (255, 255, 255)]

# TODO question:
# TODO  can 2 teams register in same chat???


class BreakStuff:
    # TODO codes len = 10
    code_teams = {'1dd7zr45fu': 't1', 'ds89dh756d0': 't2'}

    field_codes = ['1патимейкер31', '2шланг50', '3уточка17', '4гусь77', '5малинка91', '6усы43', '7трус91',
                   '8рот42', '9химера60', '10агузарова13', '11химки66', '12животное24', '13пиздоглазие22',
                   '14зигазага88', '15кандибобер85', '16сиси93', '17красава48', '18курочка11', '19палас44',
                   '20изморозь39', '21вражина25', '22смузи40', '23вейпер38', '24себяшка50', '25буратино34',
                   '26дичь70', '27банан50', '28енот89', '29бухлишко62', '30кар29', '31хитрец96', '32окошко34',
                   '33кисонька37', '34кокс12', '35кудабля33', '36тудабля76', '37чопхуеп66', '38шаболовка46',
                   '39ежик67', '40сосна95', '41тренд11', '42лайтговно17', '43мифуг54', '44хуярвел36',
                   '45геноцид56', '46папказдесь91', '47крыса37', '48братишка77', '49фрикаделька78', '50пирамидон91',
                   '51марля32', '52доширак34', '53козодой87', '54курвиметр35', '55сок69', '56бабуля46', '57сынок53',
                   '58перхоть48', '59кутикула78', '60гармонь23', '61шуба59', '62козявка60', '63язь61', '64луч46',
                   '65лом11', '66кран74', '67лосось32', '68хрен85', '69огурец81', '70бальзам52', '71загар55',
                   '72прочерк92', '73бутыль84', '74окрошка92', '75бром15', '76галстук88', '77пупочек36', '78охламон72',
                   '79морковка75', '80морж31', '81бурение82', '82бочка99', '83дклр11', '84казино65', '85люкхуюк77',
                   '86бетон99', '87боль70', '88скакалка69', '89куку28', '90скунс24', '91щи68', '92краб74',
                   '93кокошник26', '94пумба47', '95наст23', '96апломб42', '97супергуд47', '98грог87',
                   '99печенька64', '100край90']

    final_code = 'пыщпыщололовсенаабордаж5518'

    def __init__(self):
        # Players
        self.active_players = {}  # stores dict{chat_id: 'team'}
        self.finished_players = []  # stores [teams]
        self.fields = {}  # stores players fields by its ids
        self.reg_teams_cnt = 0

        # Logging
        self.db = Database()

        # font for drawing
        self.fnt = ImageFont.truetype('Verdana.ttf', 18)

        # restore sessions if any
        self.restore_session()

    def new_game(self, message):
        player_id = message.chat.id

        # fetch code from message
        try:
            code = message.text[8:]
        except (ValueError, IndexError):
            # Something wrong, try again
            return -3, None

        # == code check =========================
        if not code:
            # ignore empty codes
            return -3, None

        if code not in self.code_teams:
            # invalid code
            return -2, None
        else:
            # code ok
            team = self.code_teams[code]

            # == active/inactive team check =====
            if team in self.active_players.values():
                # playing now
                return -1, None
            elif team in self.finished_players:
                # has already played
                return 0, None
            else:
                # ok
                pass
        # == checks end =========================

        # register new player
        self.active_players[player_id] = team
        self.reg_teams_cnt += 1
        self.db.new_game(message, team, self.reg_teams_cnt)

        # create field
        self.fields[player_id] = Field(10, 10)
        self.fields[player_id].set_team(team)

        # visualize board
        output = self.draw(player_id)

        return 1, output

    def stop_game(self, player_id):
        if player_id in self.active_players:
            # TODO implement db stop game
            self.db.stop_game(self.active_players[player_id])
            self.finished_players.append(self.active_players[player_id])
            self.active_players.pop(player_id, None)
            self.fields.pop(player_id, None)
            return 1
        else:
            return 0

    def player_registered(self, message):
        player_id = message.chat.id

        if player_id in self.active_players:
            # now playing
            return 1
        else:
            return 0

    def move(self, message, code1, code2):
        player_id = message.chat.id

        # fetch ids
        if code1 not in self.field_codes or code2 not in self.field_codes:
            # invalid codes
            return -3, None, ''
        else:
            id1, id2 = self.field_codes.index(code1), self.field_codes.index(code2)

        # fetch ids positions
        brow1, bcol1 = self.fields[player_id].id_pos(id1)
        brow2, bcol2 = self.fields[player_id].id_pos(id2)

        # invalid/empty ids
        if brow1 == -1 or brow2 == -1:
            return -2, None, ''

        # Swap cells
        if self.fields[player_id].swap(brow1, bcol1, brow2, bcol2):
            # visualize board
            output = self.draw(player_id)

            # LOG
            self.db.add_move(message, self.fields[player_id].team_id,
                             len(self.fields[player_id].ids_collapsed), self.fields[player_id].collapsed_cnt,
                             self.fields[player_id].moves_cnt, id1, id2)

            if self.fields[player_id].collapsed_cnt >= 85:
                return 0, output, self.fields[player_id].stats() + '\n\n' + self.fields[player_id].list_of_collapsed()
            else:
                return 1, output, self.fields[player_id].stats() + '\n\n' + self.fields[player_id].list_of_collapsed()
        else:
            # cells not neighbours
            return -1, None, ''

    def draw(self, player_id):
        # make a blank image for the text, initialized to transparent text color
        cell = Image.new('RGB', (WIN_WIDTH, WIN_HEIGHT), BACKGROUND_COLOR)
        # get a drawing context
        d = ImageDraw.Draw(cell)
        for row in xrange(ROWS):
            for col in xrange(COLS):
                d.ellipse([col * CELL_SIZE + 5, row * CELL_SIZE + 5,
                           (col + 1) * CELL_SIZE - 5, (row + 1) * CELL_SIZE - 5],
                          COLORS[self.fields[player_id].color_at(row, col)])
                if self.fields[player_id].color_at(row, col) > -1:
                    w, h = d.textsize(str(self.fields[player_id].id_at(row, col)), font=self.fnt)
                    d.text((int((col + 0.5) * CELL_SIZE - 0.5 * w),
                            int((row + 0.5) * CELL_SIZE - 0.5 * h)),
                           str(self.fields[player_id].id_at(row, col)), font=self.fnt, fill=(0, 0, 0))
        output = StringIO()
        # cell.save(output, format="jpeg")
        # output.seek(0)
        cell.save('img' + str(player_id) + '.jpg', quality=50)

        return output

    def restore_session(self):
        # TODO restore active/finished players + active players sessions with executed moves from db
        # load list of players
        self.db.fetch_teams()
        self.active_players = {}  # stores dict{chat_id: 'team'}
        self.finished_players = []  # stores [teams]
        self.reg_teams_cnt = 0

        # load list of moves for each active player

        # for each active player replay its moves
        return
