####Description:
Match 3 game on 10x10 grid for Telegram bot. Logs moves to db.

####Requirements:
* python;  
* pytelegrambotapi (https://github.com/eternnoir/pyTelegramBotAPI);  
* cherrypy;  
* pillow (PIL);  
* sqlite3.

####Setup:
Specify Telegram bot token in config.py.  
Lists code\_teams and field\_codes can be edited in breakstuff.py.  
Field can be edited in field.py.  
Specify server ip at WEBHOOK\_HOST in bot.py.  
Create self-signed webhook sertificate and place files 'webhook\_cert.pem' and 'webhook\_pkey.pem' in script folder.  
Run 'nohup python bot.py&' on server.

####Usage:
* 'newgame team\_id' command in chat with team\_id from code\_teams list to register team and chat and start gaem;
* 'icodeI jcodeJ' command in registered chat to swap i-th and j-th tiles given that 'icodeI' is code for i-th tile and 'jcodeJ' is code for j-th tile from field\_codes list.

