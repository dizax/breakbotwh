# -*- coding: utf-8 -*-
# __author__ = 'zax'


class Field:
    def __init__(self, rows, cols):
        self.match_size = 3

        self.rows = rows
        self.cols = cols

        # 0 - red, 1 - green, 2 - blue, 3 - yellow
        self.colors_cnt = 4
        # custom colors
        self.colors = [
            [0, 2, 0, 3, 3, 0, 3, 0, 2, 3],
            [2, 3, 1, 1, 3, 3, 2, 0, 3, 3],
            [2, 0, 2, 3, 2, 1, 0, 3, 2, 1],
            [3, 2, 0, 3, 0, 1, 2, 3, 0, 1],
            [1, 1, 0, 1, 0, 2, 1, 2, 0, 3],
            [2, 2, 3, 2, 2, 1, 1, 2, 3, 2],
            [2, 3, 0, 1, 1, 3, 0, 1, 2, 0],
            [0, 1, 2, 2, 0, 3, 1, 2, 2, 0],
            [0, 2, 3, 0, 0, 1, 1, 0, 0, 3],
            [3, 1, 0, 3, 2, 1, 0, 2, 2, 0],
        ]
        # custom order (ids from 1 to 100)
        self.ids = [
            [22, 88, 20, 28, 74, 77, 15, 68, 6, 53],
            [30, 38, 32, 3, 18, 14, 76, 57, 55, 47],
            [26, 58, 87, 92, 79, 78, 72, 13, 49, 80],
            [60, 89, 37, 84, 70, 81, 69, 45, 10, 52],
            [35, 40, 54, 50, 5, 43, 39, 7, 25, 90],
            [98, 8, 46, 12, 65, 73, 94, 61, 83, 9],
            [56, 1, 34, 86, 44, 93, 91, 66, 11, 24],
            [27, 63, 59, 71, 97, 48, 62, 23, 2, 95],
            [16, 21, 75, 4, 29, 85, 51, 67, 19, 82],
            [42, 33, 100, 96, 36, 41, 99, 17, 31, 64]
        ]
        # id_pos[id] - [r,c] pos for id+1 in ids
        self.ids_pos = [
            [6, 1], [7, 8], [1, 3], [8, 3], [4, 4], [0, 8], [4, 7], [5, 1], [5, 9], [3, 8],
            [6, 8], [5, 3], [2, 7], [1, 5], [0, 6], [8, 0], [9, 7], [1, 4], [8, 8], [0, 2],
            [8, 1], [0, 0], [7, 7], [6, 9], [4, 8], [2, 0], [7, 0], [0, 3], [8, 4], [1, 0],
            [9, 8], [1, 2], [9, 1], [6, 2], [4, 0], [9, 4], [3, 2], [1, 1], [4, 6], [4, 1],
            [9, 5], [9, 0], [4, 5], [6, 4], [3, 7], [5, 2], [1, 9], [7, 5], [2, 8], [4, 3],
            [8, 6], [3, 9], [0, 9], [4, 2], [1, 8], [6, 0], [1, 7], [2, 1], [7, 2], [3, 0],
            [5, 7], [7, 6], [7, 1], [9, 9], [5, 4], [6, 7], [8, 7], [0, 7], [3, 6], [3, 4],
            [7, 3], [2, 6], [5, 5], [0, 4], [8, 2], [1, 6], [0, 5], [2, 5], [2, 4], [2, 9],
            [3, 5], [8, 9], [5, 8], [3, 3], [8, 5], [6, 3], [2, 2], [0, 1], [3, 1], [4, 9],
            [6, 6], [2, 3], [6, 5], [5, 6], [7, 9], [9, 3], [7, 4], [5, 0], [9, 6], [9, 2]
        ]

        self.collapsed_cnt = 0
        self.moves_cnt = 0

        self.collapse_by_cols = [[] for i in xrange(cols)]
        self.ids_collapsed = []

        self.team = ''
        self.team_id = 0

    def set_team(self, m_team, m_team_id):
        self.team = m_team
        self.team_id = m_team_id

    def color_at(self, row, col):
        return self.colors[row][col]

    def id_at(self, row, col):
        return self.ids[row][col]

    def id_pos(self, m_id):
        return self.ids_pos[m_id]

    def cells_left_percentage(self):
        return 1. - float(self.collapsed_cnt) / self.rows / self.cols

    def print_colors(self):
        for r in xrange(self.rows):
            print str.join(' ', ['*' if self.colors[r][c] < 0 else str(self.colors[r][c]) for c in xrange(self.cols)])

    def print_collapsed(self):
        for r in xrange(self.rows):
            print str.join(' ', ['*' if r in self.collapse_by_cols[c] else '-' for c in xrange(self.cols)])

    def stats(self):
        return "Всего камней заработано: %d\nКамней заработано за ход: %d\nВсего ходов сделано: %d" \
               % (self.collapsed_cnt, len(self.ids_collapsed), self.moves_cnt)

    def list_of_collapsed(self):
        return 'Список заработанных камней:\n' + '\n'.join([str(m_id+1) for m_id in self.ids_collapsed])

    # make a move if valid
    def swap(self, row1, col1, row2, col2):
        # check if cells  are neighbours
        neighbours = (row1 == row2 and col1 != col2 and abs(col2-col1) <= 1) or \
                     (col1 == col2 and row1 != row2 and abs(row2-row1) <= 1)

        if neighbours:
            # prohibit if id == -1 (not necessary)
            if self.ids[row1][col1] == -1 or self.ids[row2][col2] == -1:
                return False, ()

            self.moves_cnt += 1

            # swap colors
            tmp = self.colors[row1][col1]
            self.colors[row1][col1] = self.colors[row2][col2]
            self.colors[row2][col2] = tmp
            # swap ids positions
            tmp = self.ids_pos[self.ids[row1][col1]]
            self.ids_pos[self.ids[row1][col1]] = self.ids_pos[self.ids[row2][col2]]
            self.ids_pos[self.ids[row2][col2]] = tmp
            # swap ids
            tmp = self.ids[row1][col1]
            self.ids[row1][col1] = self.ids[row2][col2]
            self.ids[row2][col2] = tmp

            # collapse while can
            self.seek_matches_while_can()

            return True
        else:
            return False

    def seek_matches_while_can(self):
        self.ids_collapsed[:] = []

        while self.find_matches():
            self.collapse_matches()
            self.fall_down()
            self.fall_left()

    def find_matches(self):
        has_matches = False

        # [..., [topR, .., bottomR], ...]  (topR < bottomR)
        for c in xrange(self.cols):
            self.collapse_by_cols[c][:] = []

        # rows
        for r in xrange(self.rows):
            match_size = 1
            for c in xrange(1, self.cols):
                # avoid empty cells
                if self.colors[r][c] == -1:
                    if match_size >= self.match_size:
                        has_matches = True
                        # appropriate for fall alg
                        for cc in xrange(c - 1 - match_size + 1, c):
                            self.collapse_by_cols[cc].append(r)
                    match_size = 0
                    continue

                # match check
                if self.colors[r][c] == self.colors[r][c-1]:
                    match_size += 1
                else:
                    if match_size >= self.match_size:
                        has_matches = True
                        # appropriate for fall alg
                        for cc in xrange(c-1-match_size+1, c):
                            self.collapse_by_cols[cc].append(r)
                    match_size = 1

                # end of the row match check
                if c == self.cols-1 and match_size >= self.match_size:
                    has_matches = True
                    # appropriate for fall alg
                    for cc in xrange(c-match_size+1, c+1):
                        self.collapse_by_cols[cc].append(r)

        # cols
        for c in xrange(self.cols):
            match_size = 1
            for r in xrange(1, self.rows):
                # avoid empty cells
                if self.colors[r][c] == -1:
                    if match_size >= self.match_size:
                        has_matches = True
                        # appropriate for fall alg
                        for rr in xrange(r-1-match_size+1, r):
                            # TODO avoid this check?
                            if rr not in self.collapse_by_cols[c]:
                                self.collapse_by_cols[c].append(rr)
                    continue

                # match check
                if self.colors[r][c] == self.colors[r-1][c]:
                    match_size += 1
                else:
                    if match_size >= self.match_size:
                        has_matches = True
                        # appropriate for fall alg
                        for rr in xrange(r-1-match_size+1, r):
                            # TODO avoid this check?
                            if rr not in self.collapse_by_cols[c]:
                                self.collapse_by_cols[c].append(rr)
                    match_size = 1

                # end of the col match check
                if r == self.rows-1 and match_size >= self.match_size:
                    has_matches = True
                    # appropriate for fall alg
                    for rr in xrange(r-match_size+1, r+1):
                        # TODO avoid this check?
                        if rr not in self.collapse_by_cols[c]:
                            self.collapse_by_cols[c].append(rr)

            # appropriate for fall alg
            self.collapse_by_cols[c].sort()

        return has_matches

    def collapse_matches(self):
        for c in xrange(self.cols):
            for r in self.collapse_by_cols[c]:
                self.collapsed_cnt += 1
                self.ids_collapsed.append(self.ids[r][c])

                self.ids_pos[self.ids[r][c]] = [-1, -1]
                self.ids[r][c] = -1
                self.colors[r][c] = -1

    def fall_down(self):
        # TODO implement block falling (instead of one-by-one cell falling)
        for c in xrange(self.cols):
            if not self.collapse_by_cols[c]:
                continue

            for i, r_clear in enumerate(self.collapse_by_cols[c]):
                for r in xrange(r_clear, 0, -1):
                    self.colors[r][c] = self.colors[r-1][c]
                    self.ids[r][c] = self.ids[r-1][c]
                    if self.ids[r-1][c] != -1:
                        self.ids_pos[self.ids[r-1][c]] = [r, c]
                    else:
                        break

                if i == 0:
                    self.colors[0][c] = -1
                    self.ids[0][c] = -1

    def fall_left(self):
        res = False

        for c in xrange(self.cols-2, -1, -1):
            if self.colors[self.rows-1][c] == -1:
                for cc in xrange(c, self.cols-1):
                    if self.colors[self.rows-1][cc] == self.colors[self.rows-1][cc+1] == -1:
                        break
                    res = True

                    for r in xrange(self.rows):
                        self.colors[r][cc] = self.colors[r][cc+1]
                        self.ids[r][cc] = self.ids[r][cc+1]
                        if self.ids[r][cc+1] != -1:
                            self.ids_pos[self.ids[r][cc+1]] = [r, cc]

                if self.colors[self.rows-1][self.cols-1] != -1:
                    for r in xrange(self.rows):
                        self.colors[r][self.cols-1] = -1
                        self.ids[r][self.cols-1] = -1
        return res
